#include "AnimationSystem.h"
#include "Component.h"
void AnimationSystem::tick(ECS::World* world, float dTime) {
	world->each<Animator, Sprite>(
		[&](ECS::Entity* ent, ECS::ComponentHandle<Animator> animator, ECS::ComponentHandle<Sprite> sprite) -> void {
			sprite->sprite.setTextureRect(sf::IntRect(animator->currentCol * animator->sprWidth, animator->currentRow * animator->sprHeight, animator->sprWidth, animator->sprHeight));
			animator->currentTime += dTime;
			if (animator->currentTime >= animator->nextFrameTime) {
				animator->currentTime = 0;
				animator->currentCol = (animator->currentCol + 1) % 4;
			}
		});
}