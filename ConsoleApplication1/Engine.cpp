#include "Engine.h"
#include <iostream>
#include <SFML/Graphics.hpp>
#include "RenderingSystem.h"
Engine::Engine(void) {

}
Engine& Engine::getInstance(void) {
	static Engine Instance;
		return Instance;
}
void Engine::start(sf::RenderWindow* win) {

	window = win;
	quit = false;
	update();
}
void Engine::update(){
	while (window -> isOpen()) {
		sf::Event event;
		while (window -> pollEvent(event)) {
			if (event.type == sf::Event::Closed || quit) {
				window -> close();
			}
		}
		world->tick(10.f);
	}
}

Engine::~Engine(void) {

}