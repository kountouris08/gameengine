#include "RenderingSystem.h"
#include "ECS.h"
#include "Engine.h"
#include "Component.h"

RenderingSystem::RenderingSystem(void) {

}
RenderingSystem::~RenderingSystem(void) {

}

void RenderingSystem::tick(ECS::World* world, float dTime) {

	Engine::getInstance().window->clear();

	world->each<Transform, Sprite>(
		[&](ECS::Entity* ent,
			ECS::ComponentHandle<Transform> transform,
			ECS::ComponentHandle<Sprite> sprite) -> void {

			if (_textureMap.count(sprite->texture) < 1) {
				_textureMap[sprite->texture] = _loadTexture(sprite->texture);
			}
			if (sprite->sprite.getTexture() == NULL) {
				sprite->sprite.setTexture(*_textureMap[sprite->texture]);
				sprite->width = sprite->sprite.getGlobalBounds().width;
				sprite->height = sprite->sprite.getGlobalBounds().height;
			}
		
			sprite->sprite.setPosition(transform->X, transform->Y);
			Engine::getInstance().window->draw(sprite->sprite);
	});
	Engine::getInstance().window->display();
}
sf::Texture* RenderingSystem::_loadTexture(std::string Texture) {
	sf::Texture* text = new sf::Texture();
	if (!text->loadFromFile(Texture)) {
	std::cerr << "unable to load image." << Texture << "\. Is this image in the correct directory?" << std::endl;
	std::cerr << "Error: " << std::endl;
		system("pause");
		exit(EXIT_FAILURE);
	}
	return text;
}