#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>
#include "Engine.h"
#include "ECS.h"

struct Animator {
	ECS_DECLARE_TYPE;
	Animator(int width, int height, float timeBeetweenFrames) {
		sprWidth = width;
		sprHeight = height;
		currentCol = 0;
		currentRow = 0;
		nextFrameTime = timeBeetweenFrames;
		currentTime = 0;
	}
	int sprWidth;
	int sprHeight;
	int currentCol;
	int currentRow;
	float nextFrameTime;
	int currentTime;
	};
ECS_DEFINE_TYPE(Animator);
	struct Transform{
		ECS_DECLARE_TYPE;
		Transform(float x, float y) {
			X = x;
			Y = y;
			rotation = 0.0f;
		}
		float X;
		float Y;
		float rotation;
	};
	ECS_DEFINE_TYPE(Transform);


	struct Sprite{
		ECS_DECLARE_TYPE;
		Sprite(std::string name) {
			texture = name;
		}
		sf::Sprite sprite;
		sf::String texture;
		int width;
		int height;
	};
	ECS_DEFINE_TYPE(Sprite);


