
#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>
#include "ECS.h"
#include "RenderingSystem.h"
class Engine
{
private:
	bool quit;
	Engine(void);
	~Engine(void);
	void update();
	Engine(Engine& copy);
	Engine(Engine&& other);
	Engine& operator= (Engine& copy);
public:
	ECS::World* world;
	sf::RenderWindow* window;
	static Engine& getInstance(void);
	void start(sf::RenderWindow* win);
	void addSystem(ECS::EntitySystem* newSys) {
		world->registerSystem(newSys);
		world->enableSystem(newSys);
	}
};
